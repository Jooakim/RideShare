package com.kvjoakim.rideshare.Model

import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

/**
 * Test class for @see TripProposal.kt
 */
class TripProposalTest {

    @Test
    fun testNoOffset() {
        val start = Calendar.getInstance()
        start.set(2017,4,3,2,43)
        val end = Calendar.getInstance()
        end.set(2017,4,3,2,44)

        val trip = TripProposal(start,end)
        assertEquals("Test", 0, trip.calculateTimeOffsetStart(trip))
        assertEquals("Test", 0, trip.calculateTimeOffsetEnd(trip))

    }

    @Test
    fun testSmallOffset() {
        val start = Calendar.getInstance()
        start.set(2017,4,3,2,43,1)
        val end = Calendar.getInstance()
        end.set(2017,4,3,2,43,2)

        val trip = TripProposal(start,start)
        val trip2 = TripProposal(end,end)
        assertEquals("Test", 1, trip.calculateTimeOffsetStart(trip2)/1000)
        assertEquals("Test", 1, trip2.calculateTimeOffsetEnd(trip)/1000)

    }

    @Test
    fun testLargeOffset() {
        val start = Calendar.getInstance()
        start.set(2017,4,4,2,43,2)
        val end = Calendar.getInstance()
        end.set(2017,4,3,2,43,2)

        val trip = TripProposal(start,start)
        val trip2 = TripProposal(end,end)
        assertEquals("Test", 1, trip.calculateTimeOffsetStart(trip2)/(1000*60*60*24))
        assertEquals("Test", 1, trip2.calculateTimeOffsetEnd(trip)/(1000*60*60*24))

    }

}