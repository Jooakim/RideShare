package com.kvjoakim.rideshare.Model

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.util.*

/**
 * Test of the class HypothesisComparator
 *
 * @author Joakim Andersson
 */
class HypothesisComparatorTest {

    lateinit var queue : PriorityQueue<TripHypothesis>

    @Before
    fun setUp() {
        queue = PriorityQueue<TripHypothesis>(10,HypothesisComparator())
    }

    @Test
    fun testDifferenceInStartTime() {
        val time1 = Calendar.getInstance()
        time1.set(2017,1,1,1,1,1)
        val time2 = Calendar.getInstance()
        time2.set(2013,1,1,1,1,1)
        val smallTimeDifference = TripHypothesis(TripProposal(time1, time1), TripProposal(time1, time1))
        val bigTimeDifference = TripHypothesis(TripProposal(time1, time1), TripProposal(time2,time2))
        queue.add(bigTimeDifference)
        queue.add(smallTimeDifference)

        assertEquals("Big difference sorted before small", smallTimeDifference, queue.poll())
        assertEquals("Expected big difference", bigTimeDifference, queue.poll())
    }

    @Test
    fun testDifferenceInEndTime() {
        val time1 = Calendar.getInstance()
        time1.set(2017,1,1,1,1,1)
        val time2 = Calendar.getInstance()
        time2.set(2013,1,1,1,1,1)
        val smallTimeDifference = TripHypothesis(TripProposal(time1, time1), TripProposal(time1, time1))
        val bigTimeDifference = TripHypothesis(TripProposal(time1, time1), TripProposal(time1,time2))
        queue.add(bigTimeDifference)
        queue.add(smallTimeDifference)

        assertEquals("Big difference sorted before small", smallTimeDifference, queue.poll())
        assertEquals("Expected big difference", bigTimeDifference, queue.poll())
    }
}