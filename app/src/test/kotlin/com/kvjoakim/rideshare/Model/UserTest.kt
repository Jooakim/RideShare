package com.kvjoakim.rideshare.Model

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.Before
import java.util.*

/**
 * Test class for @see User.kt
 */
class UserTest {
    lateinit var user : User
    @Before
    fun setUp() {
        user = User("")
    }

    @Test
    fun testAddTrips() {
        user.addTrip(TripProposal(Calendar.getInstance(), Calendar.getInstance()))
        user.addTrip(TripProposal(Calendar.getInstance(), Calendar.getInstance()))
        assertEquals("Wrong number of trips recorded", 2, user.getNumberOfTrips())
    }

    @Test
    fun testRemoveTrip() {
        user.addTrip(TripProposal(Calendar.getInstance(), Calendar.getInstance()))
        val trip = TripProposal(Calendar.getInstance(), Calendar.getInstance())
        user.addTrip(trip)
        user.removeTrip(trip)
        assertEquals("Wrong number of trips recorded", 1, user.getNumberOfTrips())
    }

    @Test
    fun testAddDuplicateTrip() {
        user.addTrip(TripProposal(Calendar.getInstance(), Calendar.getInstance()))
        val trip = TripProposal(Calendar.getInstance(), Calendar.getInstance())
        user.addTrip(trip)
        user.addTrip(trip)
        assertEquals("Wrong number of trips recorded", 2, user.getNumberOfTrips())

    }

}