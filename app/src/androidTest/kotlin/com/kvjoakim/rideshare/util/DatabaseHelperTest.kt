package com.kvjoakim.rideshare.util

import android.content.Context
import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import android.support.test.InstrumentationRegistry.getInstrumentation
import com.kvjoakim.rideshare.Model.TripProposal
import com.kvjoakim.rideshare.Model.User
import org.junit.Before
import java.util.*


/**
 * Class for testing the DatabaseHelper
 */
@RunWith(AndroidJUnit4::class)
class DatabaseHelperTest {

    val testContext : Context by lazy { getInstrumentation().targetContext }
    lateinit var dbHelper : DatabaseHelper

    @Before
    fun setup() {
        dbHelper = DatabaseHelper.getInstance(testContext)!!
    }
    @Test
    @SmallTest
    fun testDatabaseConnection() {
        assertNotNull(DatabaseHelper.getInstance(testContext))

        assertEquals("Not a singleton", DatabaseHelper.getInstance(testContext),
                DatabaseHelper.getInstance(testContext))

    }

    @Test
    @SmallTest
    fun testAddUser() {
        val username = "Karl"
        val user = User(username)

        dbHelper.addUser(user)

        assertEquals("User did not get saved correctly in the database",
                user.username, dbHelper.getUser(username)?.username)
    }

    @Test
    @SmallTest
    fun testAddTrip() {

        val user = User("Test")
        dbHelper.removeUser(user)
        dbHelper.addUser(user)

        val trip = TripProposal(Calendar.getInstance(), Calendar.getInstance())
        dbHelper.addTrip(user,trip)

        val trips = dbHelper.getTrips(user)
        assertEquals("Start time not saved correctly", trip.startTime, trips.first().startTime)
        assertEquals("End time not saved correctly", trip.endTime, trips.first().endTime)
    }

    @Test
    @SmallTest
    fun testRemoveUser() {
        val user = User("Test")
        dbHelper.addUser(user)
        assertNotNull(dbHelper.getUser(user.username))

        dbHelper.removeUser(user)
        assertNull(dbHelper.getUser(user.username))
    }

    @Test
    @SmallTest
    fun testRemoveInvalidUser() {
        val user = User("TestNeverAdded")

        dbHelper.removeUser(user)
        assertNull(dbHelper.getUser(user.username))
    }

    @Test
    @SmallTest
    fun testAddDuplicateUser() {
        val user = User("TestDuplicate")
        dbHelper.addUser(user)
        dbHelper.addUser(user)
        //TODO Change something between the two add operations
        // and make sure it works as intended
    }

}