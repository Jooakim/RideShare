package com.kvjoakim.rideshare.Model

/**
 * A class for holding all possible matches between users.
 *
 * @author Joakim Andersson
 */
class TripHypothesis(val t1 : TripProposal, val t2 : TripProposal){

    fun getTimeOffsetStart() : Long {
        return t1.calculateTimeOffsetStart(t2)
    }
    fun getTimeOffsetEnd() : Long {
        return t1.calculateTimeOffsetEnd(t2)
    }

}