package com.kvjoakim.rideshare.Model

import java.util.Calendar

/**
 * Class for holding information regarding a trip
 *
 * @author Joakim Andersson
 */
class TripProposal(val startTime: Calendar, val endTime: Calendar) {

    fun calculateTimeOffsetStart(reference : TripProposal) : Long {
        val refStart = reference.startTime
        val offsetStart = startTime.timeInMillis - refStart.timeInMillis

        return Math.abs(offsetStart)
    }

    fun calculateTimeOffsetEnd(reference : TripProposal) : Long {
        val refEnd = reference.endTime
        val offsetEnd = endTime.timeInMillis - refEnd.timeInMillis

        return Math.abs(offsetEnd)
    }
}

