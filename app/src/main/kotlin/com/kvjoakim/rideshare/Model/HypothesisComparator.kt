package com.kvjoakim.rideshare.Model

/**
 * Class for comparing two different Hypothesis.
 * The comparison is based on deltas between start and end times
 *
 * @author Joakim Andersson
 */
class HypothesisComparator : Comparator<TripHypothesis> {

    /**
     * @param h1 One of the triphypothesis
     * @param h2 One of the triphypothesis
     */
    override fun compare(h1 :TripHypothesis, h2 : TripHypothesis) : Int {
        if (h1.getTimeOffsetStart() < h2.getTimeOffsetStart()) {
            return -1
        }

        if (h1.getTimeOffsetEnd() < h2.getTimeOffsetEnd()) {
            return -1
        }


        return 0
    }
}