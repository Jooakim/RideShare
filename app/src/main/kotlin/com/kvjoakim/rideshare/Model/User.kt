package com.kvjoakim.rideshare.Model

import java.util.LinkedList

/**
 * Class for holding information related to the current user
 *
 * @author Joakim Andersson
 */
class User(val username : String){

    private var trips = LinkedList<TripProposal>()

    init {
        readUserData(username)
    }

    fun addTrip(trip: TripProposal) {
        if (trips.contains(trip)) {
            //TODO: Notify user maybe?
        } else {
            trips.add(trip)
        }
    }

    fun removeTrip(trip: TripProposal) {
        trips.remove(trip)
    }

    fun getNumberOfTrips(): Int {
        return trips.size
    }

    fun getTripProposals() : LinkedList<TripProposal> {
        return trips.clone() as LinkedList<TripProposal>
    }

    fun readUserData(username: String) {
        //TODO: Connect to database and extract information, trips etc

    }

}
