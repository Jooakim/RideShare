package com.kvjoakim.rideshare.Model

import java.util.*

/**
 * The scheduler of trips.
 *
 * @author Joakim Andersson
 */
class TripScheduler {

    private val NUMBER_OF_HYPOTHESES_TO_CONSIDER = 10

    fun scheduleUser(user : User) {

        val props = user.getTripProposals()
        //TODO select proposal
        val queue = createHypotheses(props.poll())
        //TODO send the top x hypothesis to front end for user selection
    }

    fun createHypotheses(trip : TripProposal) : PriorityQueue<TripHypothesis> {
        val queue = PriorityQueue<TripHypothesis>(NUMBER_OF_HYPOTHESES_TO_CONSIDER, HypothesisComparator())

        val props = getRelevantTripProposals(trip)
        props.forEach({p -> queue.add(TripHypothesis(trip, p))})

        return queue
    }

    fun getRelevantTripProposals(trip: TripProposal) : LinkedList<TripProposal> {
        //TODO Implement
        val props = LinkedList<TripProposal>()
        return props
    }
}
