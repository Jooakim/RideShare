package com.kvjoakim.rideshare.util

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.kvjoakim.rideshare.Model.TripProposal
import com.kvjoakim.rideshare.Model.User
import java.util.*

/**
 * Class for managing CRUD operations on the database
 *
 * @author Joakim Andersson
 */
 class DatabaseHelper  constructor(context: Context) :
        SQLiteOpenHelper(context.applicationContext, DatabaseHelper.DB_NAME, null, DatabaseHelper.DB_VERSION) {

    companion object {
        val DB_NAME = "RideShare.db"
        val DB_VERSION = 2
        private var sInstance : DatabaseHelper? = null
        @Synchronized fun getInstance(context: Context): DatabaseHelper? {

            // Use the application context, which will ensure that you
            // don't accidentally leak an Activity's context.
            // See this article for more information: http://bit.ly/6LRzfx
            if (sInstance == null) {
                sInstance = DatabaseHelper(context.applicationContext)
            }
            return sInstance
        }
    }
    val USERS_TABLE_NAME = "RideShareUsers"
    val TRIPS_TABLE_NAME = "UserTrips"

//    val KEY_ID = "userId"
    val KEY_DATE = "dateJoined"
    val KEY_USERNAME = "username"

    val KEY_START_TIME = "startTime"
    val KEY_END_TIME = "endTime"
    //TODO Make use of these
//    val KEY_START_POINT = "startPoint"
//    val KEY_END_POINT = "endPoint"

    override fun onCreate(db: SQLiteDatabase?) {
        // Setup table related to user
        val CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS $USERS_TABLE_NAME (" +
                "$KEY_USERNAME TEXT PRIMARY KEY NOT NULL, " +
                "$KEY_DATE TEXT " +
                ")"
        db?.execSQL(CREATE_USERS_TABLE)

        // Setup table related to the user's trips
        val CREATE_USER_TRIPS_TABLE = "CREATE TABLE $TRIPS_TABLE_NAME(" +
                "$KEY_USERNAME TEXT, " +
                "$KEY_START_TIME TEXT, " +
                "$KEY_END_TIME TEXT, " +
//                KEY_START_POINT+ " TEXT, " +
//                KEY_END_POINT + " TEXT" +
                "FOREIGN KEY($KEY_USERNAME) REFERENCES $USERS_TABLE_NAME($KEY_USERNAME)" +
                ")"
        db?.execSQL(CREATE_USERS_TABLE)
        db?.execSQL(CREATE_USER_TRIPS_TABLE)

    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db?.execSQL("DROP TABLE IF EXISTS '$USERS_TABLE_NAME'")
        db?.execSQL("DROP TABLE IF EXISTS '$TRIPS_TABLE_NAME'")

        onCreate(db)
    }

    fun addUser(user : User) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_USERNAME, user.username)
        val cal = Calendar.getInstance()
        val date = "" + cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH) + "-" +
                cal.get(Calendar.DAY_OF_MONTH)
        values.put(KEY_DATE, date)
        db.insert(USERS_TABLE_NAME, null, values)
        db.close()
    }

    fun getUser(username : String) : User? {
        var user : User? = null
        val db = this.readableDatabase

        val cursor : Cursor = db.query(USERS_TABLE_NAME, arrayOf(KEY_DATE, KEY_USERNAME),
                KEY_USERNAME + "=?", arrayOf(username), null, null, null, null)
        if (cursor.count > 0) {
            user = User(username)
        }


        cursor.close()
        db.close()
        return user
    }

    fun removeUser(user : User) {
        val db = this.writableDatabase
        db.delete(USERS_TABLE_NAME, KEY_USERNAME + " = ?", arrayOf(user.username))
        db.delete(TRIPS_TABLE_NAME, KEY_USERNAME + " = ?", arrayOf(user.username))
        db.close()
    }

    fun addTrip(user: User, tripProposal: TripProposal) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_USERNAME, user.username)
        values.put(KEY_START_TIME, tripProposal.startTime.timeInMillis)
        values.put(KEY_END_TIME, tripProposal.endTime.timeInMillis)
        db.insert(TRIPS_TABLE_NAME, null, values)
        db.close()
    }

    fun  getTrips(user: User): Collection<TripProposal> {
        val db = this.readableDatabase

        val cursor : Cursor = db.query(TRIPS_TABLE_NAME, arrayOf(KEY_USERNAME, KEY_START_TIME, KEY_END_TIME),
                KEY_USERNAME + "=?", arrayOf(user.username), null, null, null, null)

        val trips = mutableListOf<TripProposal>()
        while (cursor.moveToNext()) {
            val calStart = Calendar.getInstance()
            calStart.timeInMillis = cursor.getString(1).toLong()
            val calEnd = Calendar.getInstance()
            calEnd.timeInMillis = cursor.getString(2).toLong()
            trips.add(TripProposal(calStart, calEnd))
        }

        cursor.close()
        db.close()
        return trips
    }

}
